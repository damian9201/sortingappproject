package com.epam.sortapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class NullSortTest {
    private Integer[] a;
    private Integer[] b;

    Sort sor = new Sort();

    public NullSortTest(Integer[] a, Integer[] b) {
        this.a = a;
        this.b = b;

    }


    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Integer[][][]{
                {{1, 2}, {null}},
                {{4, 5}, {null}},

        });
    }

    @Test
    public void testNullSortedCases() {
        try {
            sor.sortArray(b);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }


    }
}
