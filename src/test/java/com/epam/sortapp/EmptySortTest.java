package com.epam.sortapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
@RunWith(Parameterized.class)
public class EmptySortTest {
    private Integer[] a;
    private Integer[] b;

    Sort sor = new Sort();

    public EmptySortTest(Integer[] a, Integer[] b) {
        this.a = a;
        this.b = b;

    }


    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Integer[][][]{
                {{1,2,3}, {}},
                {{3,4,9}, {}},
                {{2,3,9}, {}},
                {{2,3,6,9,25}, {}}
        });
    }

    @Test
    public void testEmptyCases() {
      Integer []a={};

       assertArrayEquals("Wrong data",a,sor.sortArray(b));
    }




}

