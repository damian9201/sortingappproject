package com.epam.sortapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortedSortTest {
    private Integer[] a;
    private Integer[] b;

    Sort sor = new Sort();

    public SortedSortTest(Integer[] a, Integer[] b) {
        this.a = a;
        this.b = b;

    }


    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Integer[][][]{
                {{1, 2, 3, 6}, {1, 2, 3, 6}},
                {{1, 2, 3, 4, 5, 6}, {1, 2, 3, 4, 5, 6}},
                {{2, 3, 6, 20}, {2, 3, 6, 20}},
                {{1, 2, 3, 20, 22, 36}, {1, 2, 3, 20, 22, 36}}
        });
    }

    @Test
    public void testSortedCases() {

        assertArrayEquals(a, sor.sortArray(b));
    }


}

