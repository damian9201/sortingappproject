package com.epam.sortapp;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class NotSortedSortTest {
    private Integer[] a;
    private Integer[] b;

    Sort sor = new Sort();

    public NotSortedSortTest(Integer[] a, Integer[] b) {
        this.a = a;
        this.b = b;

    }


    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Integer[][][]{
                {{1, 2, 3, 6}, {1, 2, 6, 3}},
                {{1, 2, 3, 4, 5, 6}, {5, 4, 1, 2, 6, 3}},
                {{2, 3, 6, 20}, {20, 2, 6, 3}},
                {{1, 2, 3, 20, 22, 36}, {1, 2, 20, 36, 22, 3}}
        });
    }

    @Test
    public void testOtherCases() {

        assertArrayEquals(a, sor.sortArray(b));
    }




}
