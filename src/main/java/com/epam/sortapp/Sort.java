package com.epam.sortapp;

import java.util.Arrays;

public class Sort {
        public static void main(String[] args) {
            Sort s=new Sort();
        Integer[]a=new Integer[args.length];
            for (int i = 0; i < args.length ; i++) {
                a[i]=Integer.parseInt(args[i]);
            }
            s.sortArray(a);
            System.out.println(Arrays.toString(a));
    }
    public static Integer [] sortArray(Integer[] array){
        if(array==null){
            throw new IllegalArgumentException();
        }
        else if(array.length==0) {
            throw new IllegalArgumentException();
        }

        else {
            Arrays.sort(array);
            return array;
        }
    }
    }


